INSERT INTO ingredient VALUES ( "580e9980-e29b-11d4-a716-446655490000", 1337.1337, 1338.1338 , "testcategr", 123.123 , "testing" , 66.66 );
SELECT * FROM ingredient WHERE id = "580e9980-e29b-11d4-a716-446655490000";


INSERT INTO recipe VALUES ( "580e9970-e29b-11d4-a716-446655444000", "cat" , "dir" , true , 3 , "/pizza.jpg", "pizza formaggi" , 1 , 30 );
SELECT * FROM recipe WHERE id = "580e9970-e29b-11d4-a716-446655444000";


INSERT INTO recipe_amounts VALUES ( "580e9970-e29b-11d4-a746-446655447000" , 1447.1447);
SELECT * FROM recipe_amounts WHERE recipe_id = "580e9970-e29b-11d4-a746-446655447000";


INSERT INTO recipe_amounttypes VALUES ( "580e9970-e29b-11d4-a716-44665527000" , "kg");
SELECT * FROM recipe_amounttypes WHERE recipe_id = "580e9970-e29b-11d4-a716-44665527000" ;


INSERT INTO recipe_ingredients VALUES ( "580e9970-e29b-11d4-a716-446655444000" , "580e9980-e29b-11d4-a716-446655490000" );
SELECT * FROM recipe_ingredients WHERE recipe_id = "580e9970-e29b-11d4-a716-446655444000";



DELETE FROM recipe_ingredients WHERE true;
DELETE FROM recipe WHERE true;
DELETE FROM ingredient WHERE true;
DELETE FROM recipe_amounts WHERE true;
DELETE FROM recipe_amounttypes WHERE true;