package knuth;

import java.util.UUID;

public class Ingredient{

	private UUID id;
	
	private String name;
	
	private String category;
	
	private float calories;
	
	private float protein;
	
	private float carbohydrates;
	
	private float fat;
	
	
    public UUID getId() {
		return id;
	}

    
	public void setId(UUID id) {
		this.id = id;
	}

    
	public String getName() {
		return name;
	}

    
	public void setName(String name) {
		this.name = name;
	}

   
	public String getCategory() {
		return category;
	}

    
	public void setCategory(String category) {
		this.category = category;
	}

    
	public float getCalories() {
        return this.calories;
    }

    
    public void setCalories(float calories) {
        this.calories = calories;
    }

    
    public float getProtein() {
        return this.protein;
    }

    
    public void setProtein(float protein) {
        this.protein = protein;
    }

    
    public float getCarbohydrates() {
        return this.carbohydrates;
    }

    
    public void setCarbohydrates(float carbohydrates) {
        this.carbohydrates = carbohydrates;
    }
	
    
    public float getFat() {
        return this.fat;
    }

   
    public void setFat(float fat) {
        this.fat = fat;
    }
	
	public String toString() {
		return this.name+ " "+ this.category + " " + this.id.toString();
	}

}