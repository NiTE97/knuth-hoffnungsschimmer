package knuth;

import java.io.File;
import java.sql.*;
import java.util.*;

public class App
{
	private void SQLqueryTest() {
		
		SqliteQuerys queryObj = new SqliteQuerys();
		queryObj.connectDatabase();
		queryObj.setupTables();

		
		queryObj.executeRecipeIngredientsInsert(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"),
												UUID.fromString("580e9780-e29b-11d4-a716-446656490000"));
		
		queryObj.executeRecipeAmounttypesInsert(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"),
												"kg");
		
		queryObj.executeRecipeAmountsInsert(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"), 
											(float) 7.7);
		
		queryObj.executeInsertRecipeQuery(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"),
											"imagine-dish",
											"straight",
											true,
											3,
											"/imagine.png",
											"Super Hexa Food",
											33,
											44);
											  
		queryObj.executeInsertIngredientQuery(UUID.fromString("580e9780-e29b-11d4-a716-446656490000"),
											  (float)888.888,
											  (float)666.666,
											  "imaginary",
											  (float)777.123,
											  "Hexfruit",
											  (float)133.337);
											  
		List<UUID> id_list = queryObj.executeSelectIngredientIDsByRecipeID(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));								  
											  
	    int i = 0;
		while (!id_list.isEmpty() && id_list.size() > i ) {
			System.out.println("executeSelectIngredientIDsByRecipeID: "+id_list.toArray()[i].toString());
			i++;
		}
		
		List<Float> amounts_list = queryObj.executeSelectAmountsByRecipeID(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		i = 0;
		while(!amounts_list.isEmpty() && amounts_list.size() > i ) {
			System.out.println("executeSelectAmountsByRecipeID: " +amounts_list.toArray()[i].toString() );
			i++;
		}
		
		List<String> amounttypes_list = queryObj.executeSelectAmounttypesByRecipeID(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		i = 0;
		while(!amounttypes_list.isEmpty() && amounttypes_list.size() > i ) {
			System.out.println("executeSelectAmounttypesByRecipeID: "+amounttypes_list.toArray()[i]);
			i++;
		}
		
		Ingredient ingObj = queryObj.executeSelectIngredientByID(UUID.fromString("580e9780-e29b-11d4-a716-446656490000"));
		System.out.println("executeSelectIngredientByID: "+ingObj);
		
		Recipe recObj = queryObj.executeSelectRecipeByID(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		System.out.println("executeSelectRecipeByID: "+recObj);
		
		queryObj.executeInsertRecipeQuery(UUID.fromString("180e8970-e29b-11d4-a716-446655447000"),
											"imagine-dish",
											"straight",
											true,
											3,
											"/imagine.png",
											"Super Hexa Food",
											33,
											44);
											  
		queryObj.executeInsertIngredientQuery(UUID.fromString("280e9780-f29b-11d4-e716-446656490000"),
											  (float)888.888,
											  (float)666.666,
											  "imaginary",
											  (float)777.123,
											  "Hexfruit",
											  (float)133.337);
		queryObj.executeInsertRecipeQuery(UUID.fromString("380e9970-e29b-11d4-e716-446655447000"),
											"imagine-dish",
											"straight",
											true,
											3,
											"/imagine.png",
											"Super Hexa Food",
											33,
											44);
											  
		queryObj.executeInsertIngredientQuery(UUID.fromString("480e9780-e29b-11d4-e716-446676490000"),
											  (float)888.888,
											  (float)666.666,
											  "imaginary",
											  (float)777.123,
											  "Hexfruit",
											  (float)133.337);
		
		
		List<Ingredient> ingredients_list = queryObj.executeSelectAllIngredient();
		i = 0;
		while(!ingredients_list.isEmpty() && ingredients_list.size() > i ){
			System.out.println("All Ingredients : "+ ingredients_list.toArray()[i].toString() );
			i++;
		}
		
		List<Recipe> recipe_list = queryObj.executeSelectAllRecipe();
		i = 0;
		while(!recipe_list.isEmpty() && recipe_list.size() > i ){
			System.out.println("All Recipes : "+ recipe_list.toArray()[i].toString() );
			i++;
		}
		
		System.out.println("1");
		queryObj.executeDeleteIngredientById(UUID.fromString("580e9780-e29b-11d4-a716-446656490000"));
		System.out.println("2");
		queryObj.executeDeleteRecipeById(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		System.out.println("3");
		queryObj.executeDeleteRecipeAmountsById(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		System.out.println("4");
		queryObj.executeDeleteRecipeAmounttypesById(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		System.out.println("5");
		queryObj.executeDeleteRecipeIngredientsById(UUID.fromString("580e9970-e29b-11d4-a716-446655447000"));
		
		
		queryObj.disconnectDatabase();
		
		System.out.println("ran.");
		
	}
	
	
    public static void main( String[] args )
    {
		MainFx.main(args);
    }
}
