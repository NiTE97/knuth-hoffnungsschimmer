package knuth;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;


public class Recipe {
	
	private UUID id;
	
	private String name;
	
	private String category;
	
	private int healthrating;
	
	private int time;
	
	private int personcount;
	
	private List<Ingredient> ingredients;
	
	private List<Float> amounts;
	
	private List<String> amounttypes;
	
	private String directions;
	
	private String imagePath;
	
	private boolean favorized;
	
	
    public String getName() {
        return this.name;
    }

    
    public UUID getId() {
		return id;
	}

    
	public void setId(UUID id) {
		this.id = id;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public String getCategory() {
        return this.category;
    }

    
    public void setCategory(String category) {
        this.category = category;
    }

    
    public int getHealthRating() {
        return this.healthrating;
    }

    
    public void setHealthRating(int healthRating) {
        this.healthrating = healthrating;
    }

    
    public int getTime() {
        return this.time;
    }

    
    public void setTime(int time) {
        this.time = time;
    }

    
    public int getPersonCount() {
		return this.personcount;
	}

    
    public void setPersonCount(int personcount) {
		this.personcount = personcount;
	}

    
    public void setIngredients(List<Ingredient> ingredients) {
       this.ingredients = ingredients;
    }

    
    public List<Ingredient> getIngredients(){
        return this.ingredients;
    }

    
    public void setAmounts(List<Float> amounts){
        this.amounts = amounts;
    }

    
    public List<Float> getAmounts(){
        return this.amounts;
    }

    
    public void setAmounttypes(List<String> amounttypes){
        this.amounttypes = amounttypes;
    }

    
    public List<String> getAmounttypes(){
        return this.amounttypes;
    }

    
    public String getDirections() {
        return this.directions;
    }

    
    public void setDirections(String directions) {
        this.directions = directions;
    }

    
    public String getImagePath() {
        return this.imagePath;
    }

    
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    
    public boolean getFavorized() {
        return this.favorized;
    }

    
    public void setFavorized(boolean favorized) {
        this.favorized = favorized;
    }
	
	public String toString() {
		return this.name + " " + this.category + " " + this.directions + " " + this.imagePath;	
	}

	
}
