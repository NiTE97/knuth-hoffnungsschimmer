package knuth;

import java.sql.*;
import java.util.*;

public class SqliteQuerys{
	
	private String jdbcURL= "jdbc:sqlite:kochbuch.db";
	private Connection connection = null;
	
	public void SqliteQuerys(){
		
	}	
		
	public void connectDatabase(){
		try{
			Class.forName("org.sqlite.SQLiteJDBCLoader");
        } catch (Exception e){
			System.out.println("JDBC Driver failed..");
		}
		try{
			connection = DriverManager.getConnection(jdbcURL);
		}catch(SQLException e){
			System.out.println("Error connecting to SQLite database");
			e.printStackTrace();
		}
	}
	
	
	
	public void disconnectDatabase(){
		try{
			if (this.connection != null)
				connection.close();
		}catch(SQLException e) {
			System.out.println("Error disconnecting SQLite database");
			e.printStackTrace();
		}
	}
	
	public void setupTables(){
		
		try{
				String sql = "create table ingredient (id binary not null, calories float check (calories>=0), carbohydrates float check (carbohydrates>=0), category varchar(255) not null, fat float check (fat>=0), name varchar(255) not null, protein float check (protein>=0), primary key (id))";
				PreparedStatement preparedstatement = connection.prepareStatement(sql);
				preparedstatement.execute();
				
			} catch ( SQLException e) {
				System.out.println("Error executing Query: ");
				e.printStackTrace();
			

		}
		
		try{
				String sql = "create table recipe (id binary not null, category varchar(255) not null, directions varchar(255) not null, favorized boolean, healthrating integer check (healthrating>=0 AND healthrating<=5), image_path varchar(255), name varchar(255) not null, personcount integer not null check (personcount>=1), time integer not null check (time>=0), primary key (id))";
				PreparedStatement preparedstatement = connection.prepareStatement(sql);
				preparedstatement.execute();
				
			} catch ( SQLException e) {
				System.out.println("Error executing Query: ");
				e.printStackTrace();
			

		}
		
		try{
				String sql = "create table recipe_amounts (recipe_id binary not null, amounts float)";
				PreparedStatement preparedstatement = connection.prepareStatement(sql);
				preparedstatement.execute();
				
			} catch ( SQLException e) {
				System.out.println("Error executing Query: ");
				e.printStackTrace();
			

		}
		
		try{
				String sql = "create table recipe_amounttypes (recipe_id binary not null, amounttypes varchar(255))";
				PreparedStatement preparedstatement = connection.prepareStatement(sql);
				preparedstatement.execute();
				
			} catch ( SQLException e) {
				System.out.println("Error executing Query: ");
				e.printStackTrace();
			

		}
		
		try{
				String sql = "create table recipe_ingredients (recipe_id binary not null, ingredients_id binary not null)";
				PreparedStatement preparedstatement = connection.prepareStatement(sql);
				preparedstatement.execute();
				
			} catch ( SQLException e) {
				System.out.println("Error executing Query: ");
				e.printStackTrace();
			

		}
	}
	
		
		
	public void executeRecipeIngredientsInsert(UUID recipe_id, UUID ingredient_id){	
	 try{
			String sql = "INSERT INTO recipe_ingredients VALUES(?,?)";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,recipe_id.toString());
			preparedstatement.setString(2,ingredient_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	
	public void executeRecipeAmounttypesInsert(UUID id, String amounttype){	
	 try{
			String sql = "INSERT INTO recipe_amounttypes VALUES(?,?)";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,id.toString());
			preparedstatement.setString(2,amounttype);
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public void executeRecipeAmountsInsert(UUID id, float amounts){	
	 try{
			String sql = "INSERT INTO recipe_amounts VALUES(?,?)";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,id.toString());
			preparedstatement.setFloat(2,amounts);
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	//INSERT INTO recipe VALUES ( "580e9970-e29b-11d4-a716-446655444000", "cat" , "dir" , true , 3 , "/pizza.jpg", "pizza formaggi" , 1 , 30 );
	public void executeInsertRecipeQuery( UUID id,
										  String category,
										  String directions,
										  boolean favorized,
										  int healthrating,
										  String image_path,
										  String name,
										  int personcount,
										  int time) 
	{
	 try{
			String sql = "INSERT INTO recipe VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,id.toString());
			preparedstatement.setString(2,category);
			preparedstatement.setString(3,directions);
			preparedstatement.setBoolean(4,favorized);
			preparedstatement.setInt(5,healthrating);
			preparedstatement.setString(6,image_path);
			preparedstatement.setString(7,name);
			preparedstatement.setInt(8,personcount);
			preparedstatement.setInt(9,time);
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	//INSERT INTO ingredient VALUES ( "580e9980-e29b-11d4-a716-446655490000", 1337.1337, 1338.1338 , "testcategr", 123.123 , "testing" , 66.66 );
	public void executeInsertIngredientQuery( UUID id,
											  float calories,
											  float carbohydrates,
											  String category,
											  float fat,
											  String name,
											  float protein) 
	{
	 try{
			String sql = "INSERT INTO ingredient VALUES (?, ?, ?, ?, ?, ?, ?);";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,id.toString());
			preparedstatement.setFloat(2,calories);
			preparedstatement.setFloat(3,carbohydrates);
			preparedstatement.setString(4,category);
			preparedstatement.setFloat(5,fat);
			preparedstatement.setString(6,name);
			preparedstatement.setFloat(7,protein);
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public List<Float> executeSelectAmountsByRecipeID(UUID recipe_id){
	try{
			String sql = "SELECT * FROM recipe_amounts WHERE recipe_id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,recipe_id.toString());
			ResultSet result = preparedStatement.executeQuery();
			List<Float> retResult = new ArrayList<Float>();
			
			while(result.next()) {
				retResult.add(result.getFloat("amounts"));
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new ArrayList<Float>();
	}
	
	public List<String> executeSelectAmounttypesByRecipeID(UUID recipe_id){
	try{
			String sql = "SELECT * FROM recipe_amounttypes WHERE recipe_id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,recipe_id.toString());
			ResultSet result = preparedStatement.executeQuery();
			List<String> retResult = new ArrayList<String>();
			
			while(result.next()) {
				retResult.add(result.getString("amounttypes"));
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	public List<UUID> executeSelectIngredientIDsByRecipeID(UUID recipe_id){
	try{
			String sql = "SELECT * FROM recipe_ingredients WHERE recipe_id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,recipe_id.toString());
			ResultSet result = preparedStatement.executeQuery();
			List<UUID> retResult = new ArrayList<UUID>();
			
			while(result.next()) {
				UUID current = UUID.fromString(result.getString("ingredients_id"));
				retResult.add(current);
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new ArrayList<UUID>();
	}
	
	public List<Ingredient> executeSelectAllIngredient(){
	try{
			String sql = "SELECT * FROM ingredient";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			ResultSet result = preparedStatement.executeQuery();
			List<Ingredient> retResult = new ArrayList<Ingredient>();
			
			while(result.next()) {
				Ingredient curResult = new Ingredient();
				curResult.setId( UUID.fromString(result.getString("id")));
				curResult.setCalories(result.getFloat("calories"));
				curResult.setCarbohydrates(result.getFloat("carbohydrates"));
				curResult.setCategory(result.getString("category"));
				curResult.setFat(result.getFloat("fat"));
				curResult.setName(result.getString("name"));
				curResult.setProtein(result.getFloat("protein"));
				retResult.add(curResult);
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new ArrayList<Ingredient>();
	}
	
	
	public Ingredient executeSelectIngredientByID(UUID ingredient_id){
	try{
			String sql = "SELECT * FROM ingredient WHERE id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,ingredient_id.toString());
			ResultSet result = preparedStatement.executeQuery();
			Ingredient retResult = new Ingredient();
			
			while(result.next()) {
				retResult.setId( UUID.fromString(result.getString("id")));
				retResult.setCalories(result.getFloat("calories"));
				retResult.setCarbohydrates(result.getFloat("carbohydrates"));
				retResult.setCategory(result.getString("category"));
				retResult.setFat(result.getFloat("fat"));
				retResult.setName(result.getString("name"));
				retResult.setProtein(result.getFloat("protein"));
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new Ingredient();
	}
	
	
	public Recipe executeSelectRecipeByID(UUID recipe_id){
	try{
			String sql = "SELECT * FROM recipe WHERE id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,recipe_id.toString());
			ResultSet result = preparedStatement.executeQuery();
			Recipe retResult = new Recipe();
			
			while(result.next()) {
				retResult.setId( UUID.fromString(result.getString("id")));
				retResult.setCategory(result.getString("category"));
				retResult.setDirections(result.getString("directions"));
				retResult.setFavorized(result.getBoolean("favorized"));
				retResult.setHealthRating(result.getInt("healthrating"));
				retResult.setImagePath(result.getString("image_path"));
				retResult.setName(result.getString("name"));
				retResult.setPersonCount(result.getInt("personcount"));
				retResult.setTime(result.getInt("time"));
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new Recipe();
	}
	
	public List<Recipe> executeSelectAllRecipe(){
	try{
			String sql = "SELECT * FROM recipe";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			ResultSet result = preparedStatement.executeQuery();
			
			List<Recipe> retResult = new ArrayList<Recipe>();
			
			while(result.next()) {
				Recipe curResult = new Recipe();
				curResult.setId( UUID.fromString(result.getString("id")));
				curResult.setCategory(result.getString("category"));
				curResult.setDirections(result.getString("directions"));
				curResult.setFavorized(result.getBoolean("favorized"));
				curResult.setHealthRating(result.getInt("healthrating"));
				curResult.setImagePath(result.getString("image_path"));
				curResult.setName(result.getString("name"));
				curResult.setPersonCount(result.getInt("personcount"));
				curResult.setTime(result.getInt("time"));
				retResult.add(curResult);
			}
			
			return retResult;
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}
		return new ArrayList<Recipe>();
	}
	
	/**
	DELETE FROM ingredient WHERE id = "580e9780-e29b-11d4-a716-446656490000";
	DELETE FROM recipe WHERE id = "580e9970-e29b-11d4-a716-446655447000";
	DELETE FROM recipe_amounts WHERE recipe_id = "580e9970-e29b-11d4-a716-446655447000";
	DELETE FROM recipe_amounttypes WHERE recipe_id = "580e9970-e29b-11d4-a716-446655447000";
	DELETE FROM recipe_ingredients WHERE recipe_id = "580e9970-e29b-11d4-a716-446655447000";
	
	**/
	
	public void executeDeleteRecipeIngredientsById(UUID recipe_id){	
	 try{
			String sql = "DELETE FROM recipe_ingredients WHERE recipe_id = ?";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,recipe_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public void executeDeleteRecipeAmounttypesById(UUID recipe_id){	
	 try{
			String sql = "DELETE FROM recipe_amounttypes WHERE recipe_id = ?";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,recipe_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public void executeDeleteRecipeAmountsById(UUID recipe_id){	
	 try{
			String sql = "DELETE FROM recipe_amounts WHERE recipe_id = ?";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,recipe_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public void executeDeleteRecipeById(UUID recipe_id){	
	 try{
			String sql = "DELETE FROM recipe WHERE id = ?";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,recipe_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}
	
	public void executeDeleteIngredientById(UUID ingredient_id){	
	 try{
			String sql = "DELETE FROM ingredient WHERE id = ?";
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1,ingredient_id.toString());
			preparedstatement.execute();
			
		} catch ( SQLException e) {
			System.out.println("Error executing Query: ");
			e.printStackTrace();
		}

	}

		
		
}