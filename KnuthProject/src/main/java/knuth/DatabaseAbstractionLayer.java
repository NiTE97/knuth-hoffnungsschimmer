package knuth;

import java.util.*;
import knuth.NameSort;

public class DatabaseAbstractionLayer {
	
	private SqliteQuerys querys;
	
	public void DataBaseAbstractionLayer(){
		querys = new SqliteQuerys();
		querys.connectDatabase();
		querys.setupTables();
	}
	
	public void ShutdownBackend(){
		querys.disconnectDatabase();
	}

    public List<Recipe> getAllRecipesSortedByName () {
		List<Recipe> recipes = querys.executeSelectAllRecipe();

		Collections.sort(recipes, new NameSort());

		return recipes;
		



		}

    public List<Recipe> getAllRecipesSortedByNewest() {
        List<Recipe> recipes = querys.executeSelectAllRecipe();
		List<Ingredient> ingredients = querys.executeSelectAllIngredient();
		
		for (int i = 0; i < recipes.size(); i++) {
			
			UUID curID = recipes.get(i).getId();
			recipes.get(i).setAmounts(querys.executeSelectAmountsByRecipeID(curID));
			recipes.get(i).setAmounttypes(querys.executeSelectAmounttypesByRecipeID(curID));
			
			List<Ingredient> curIngredientList = new ArrayList<Ingredient>();
			
			List<UUID> ingredient_IDs = querys.executeSelectIngredientIDsByRecipeID(curID);
			for (int j = 0; j < ingredients.size(); j++) {
				for(int jj = 0; jj < ingredient_IDs.toArray().length;jj++) {
					
					if(((UUID)ingredient_IDs.toArray()[jj]).compareTo(ingredients.get(j).getId())==0)
						curIngredientList.add(ingredients.get(j));
				}
			}
			
			recipes.get(i).setIngredients(curIngredientList);
		}
		// Sort.
		
		List<Recipe> retRecipeList = new ArrayList<Recipe>();
		
		for ( int ii = recipes.size() - 1 ; ii >= 0 ; ii-- ){
			retRecipeList.add(recipes.get(ii));	
		}
	
		return retRecipeList;
		
    }

    public List<Recipe> getFavoriteRecipes () {
        Recipe[] allRecipes = (Recipe[])getAllRecipesSortedByNewest().toArray();
		List<Recipe> retRecipeList = new ArrayList<Recipe>();
		for (int i = 0; i < allRecipes.length; i++)
			if(allRecipes[i].getFavorized())
				retRecipeList.add(allRecipes[i]);
		return retRecipeList;
    }

    public List<Recipe> getHealthyRecipes () {
		Recipe[] allRecipes = (Recipe[])getAllRecipesSortedByNewest().toArray();
		List<Recipe> retRecipeList = new ArrayList<Recipe>();
		for (int i = 0; i < allRecipes.length; i++)
			if(allRecipes[i].getHealthRating()>3)
				retRecipeList.add(allRecipes[i]);
		return retRecipeList;
    }

    public void deleteRecipe(UUID recipe_id){
		 try {
		UUID[] ingredientIDs = (UUID[])querys.executeSelectIngredientIDsByRecipeID(recipe_id).toArray();
		for(int i = 0; i<ingredientIDs.length; i++)
			querys.executeDeleteIngredientById(ingredientIDs[i]);
		
		querys.executeDeleteRecipeIngredientsById(recipe_id);
		querys.executeDeleteRecipeAmounttypesById(recipe_id);
		querys.executeDeleteRecipeAmountsById(recipe_id);
		querys.executeDeleteRecipeById(recipe_id);
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		
    }

	// combines update & save & create
    public void saveRecipe(Recipe newrecipe){
		
		deleteRecipe(newrecipe.getId());
		
		List<Ingredient> ingredients = newrecipe.getIngredients();
		
		for (int i = 0; i < ingredients.size(); i++) {
		 
			querys.executeRecipeIngredientsInsert(newrecipe.getId(), ingredients.get(i).getId());
		
			querys.executeInsertIngredientQuery( ingredients.get(i).getId(),
											     ingredients.get(i).getCalories(),
											     ingredients.get(i).getCarbohydrates(),
											     ingredients.get(i).getCategory(),
											     ingredients.get(i).getFat(),
											     ingredients.get(i).getName(),
											     ingredients.get(i).getProtein()); 
		}
		for (int ii = 0; ii < newrecipe.getAmounts().toArray().length;ii++){
			querys.executeRecipeAmountsInsert(newrecipe.getId(), ((float)newrecipe.getAmounts().toArray()[ii]));
			querys.executeRecipeAmounttypesInsert(newrecipe.getId(), ((String)newrecipe.getAmounttypes().toArray()[ii]));
		}
		
		querys.executeInsertRecipeQuery( newrecipe.getId(),
										  newrecipe.getCategory(),
										  newrecipe.getDirections(),
										  newrecipe.getFavorized(),
										  newrecipe.getHealthRating(),
										  newrecipe.getImagePath(),
										  newrecipe.getName(),
										  newrecipe.getPersonCount(),
										  newrecipe.getTime());
										  
										  
		
    }

    public Recipe getRecipe (UUID recipe_id){
		UUID[] ingredientIDs = (UUID[])querys.executeSelectIngredientIDsByRecipeID(recipe_id).toArray();
		List<Ingredient> ingredientsList = new ArrayList<Ingredient>();
		
		for (int i = 0; i < ingredientIDs.length; i++) {
			ingredientsList.add(querys.executeSelectIngredientByID(ingredientIDs[i]));
		}
		Recipe retVal = querys.executeSelectRecipeByID(recipe_id);
		
		List<String> amounttypesList = querys.executeSelectAmounttypesByRecipeID(recipe_id);
		List<Float>  amountsList = querys.executeSelectAmountsByRecipeID(recipe_id);
        
		retVal.setAmounts(amountsList);
		retVal.setAmounttypes(amounttypesList);
		retVal.setIngredients(ingredientsList);
		
		return retVal;
    }


    public Recipe createRecipe(UUID id, String name, String category, int healthrating,
							   int time, int personcount, List<Ingredient> ingredients,
							   List<Float> amounts, List<String> amounttypes, String directions,
							   String imagePath, boolean favorized) {
        
		Recipe retVal = new Recipe();
		
		retVal.setId(id);
		retVal.setName(name);
		retVal.setCategory(category);
		retVal.setHealthRating(healthrating);
		retVal.setTime(time);
		retVal.setPersonCount(personcount);
		retVal.setIngredients(ingredients);
		retVal.setAmounts(amounts);
		retVal.setAmounttypes(amounttypes);
		retVal.setDirections(directions);
		retVal.setImagePath(imagePath);
		retVal.setFavorized(favorized);
		
		return retVal;
		
    }


    
}