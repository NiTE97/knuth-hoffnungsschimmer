module KnuthProject{
    requires javafx.controls;
	requires javafx.fxml;
	requires java.desktop;
	requires transitive javafx.graphics;
	requires javafx.base; 
	requires java.sql;
	//requires hibernate.jpa;
    exports knuth.view.fx.controllers;
	exports knuth to javafx.graphics;
    opens knuth.view.fx.controllers to javafx.fxml, javafx.graphics;
}